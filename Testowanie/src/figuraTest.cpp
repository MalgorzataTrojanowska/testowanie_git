/*
 * figury.cpp
 *
 *  Created on: 20 cze 2020
 *      Author: student
 */

#include "figura.h"
#include <iostream>
#include "../googleTestLib/gtest/gtest.h"

TEST(figuraTest, ZeroInitialized){
	figura fig;
	EXPECT_EQ("", fig.getNazwa());
	EXPECT_EQ("", fig.getKolor());
	EXPECT_EQ(0.0, fig.getPole());
	EXPECT_EQ(0, fig.getListaSize());
}

TEST(figuraTest, FullyInitialized){
	std::string nazwa = "nazwa1", kolor ="kolor1";
	double pole = 1.0;
	figura fig(nazwa, pole, kolor);
		EXPECT_EQ(nazwa, fig.getNazwa());
		EXPECT_EQ(kolor, fig.getKolor());
		EXPECT_EQ(pole, fig.getPole());
		EXPECT_EQ(1, fig.getListaSize());
}

TEST(figuraTest, Zapis){
	figura fig;
	double rozmiar = 3.5;
	std::string nazwa = "Kolo", kolor ="zielone";
	fig.zapis(nazwa, rozmiar, kolor);
	EXPECT_EQ(1, fig.getListaSize());
}

TEST(figuraTest, GetListaSize){
	figura fig;
	double rozmiar = 3.5;
	std::string nazwa = "Kolo", kolor ="zielone";
	fig.zapis(nazwa, rozmiar, kolor);
	fig.zapis("Kwadrat", 5.5, "niebieski");
	EXPECT_EQ(2, fig.getListaSize());
}

int main (int argc, char* argv[]) {
	testing::InitGoogleTest(&argc, argv);
	std::cout << "Starting test ..." << std::endl << std::flush;
	int res = RUN_ALL_TESTS();
	std::cout << std::endl << "RUN_ALL_TESTS(): " << res;
	return 0;
}







